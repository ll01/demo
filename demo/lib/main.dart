import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'dart:math';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.red,
      ),
      home: MyHomePage(title: 'Swipe'),
    );
  }
}

class PulsateCurve extends Curve {
  static bool _lock = false;
  static double i = 0;
  @override
  double transform(double t) {
    if (t > 0.95) {
      _lock = true;
    }
    if (_lock == true && t == 0) {
      _lock = false;
      i = 0;
    } else {
      i += t;
    }

    return i;
  }
}

class FadeContainer extends AnimatedWidget {
  static final _opacityTween = new CurveTween(curve: Curves.linear);
  final FractionalOffset alignment;
  Widget child;

  FadeContainer({
    Key key,
    this.alignment: FractionalOffset.center,
    Animation<double> animation,
    this.child,
  }) : super(key: key, listenable: animation);
  @override
  Widget build(BuildContext context) {
    final Animation<double> animation = listenable;
    final Matrix4 transform = Matrix4.translationValues(
        0, PulsateCurve().transform(animation.value) * -10, 0);
    return new Transform(
        alignment: alignment,
        transform: transform,
        child:
            Opacity(opacity: _opacityTween.evaluate(animation), child: child));
  }


}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> with TickerProviderStateMixin {
  int _counter = 0;
  var _viewableChairs = [0, 1];
  CarouselSlider _slider;
  AnimationController _controller;
  AnimationController _controllerone;
  Positioned _faderPosition;
  int transitionSpeed = 500;
  double iconSize = 50;

  FadeContainer _fader;

  Curve _pageCurve = Curves.decelerate;

  double _ttop = 250;

  List map(List list, converter(x)) {
    final List result = [];
    for (final x in list) {
      result.add(converter(x));
    }
    return result;
  }
    void startFadeInAndOut(AnimationController controller) {
 controller .forward().then((_) {
      controller.reverse();
    });
  } 

  List<Widget> _generateImageList() {
    return map(
        _viewableChairs,
        (x) => Stack(
              children: <Widget>[
                GestureDetector(
                  onHorizontalDragEnd: (dragDetails) {
                    _onHorizontalSwipeAction(dragDetails);
                  },
                  onVerticalDragEnd: (dragDetails) {
                     _onUpSwipeAction(dragDetails);
                  },

                  child: SlideTransition(
                      position:
                          Tween(begin: Offset(0.0, 0.0), end: Offset(0.0, -1.0))
                              .animate(_controller),
                      child: Center(
                        child: Container(
                            height: 300,
                            child: Image.asset(
                                "images/chair" + x.toString() + ".jpeg")),
                      )),
                )
              ],
            )).cast();
  }

  void calclulateChairlist(page) {
    var currentChair = _viewableChairs[page];
    _viewableChairs.remove(currentChair);
    _viewableChairs.insert(page, nextChair(currentChair));
  }

  int nextChair(int chairNumber) {
    chairNumber++;
    if (chairNumber > 2) {
      chairNumber = 0;
    }
    return chairNumber;
  }

  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }



  void setFade(IconData icon, double left, double right) {
    setState(() {
      _fader.child = Icon(icon, size: iconSize);
      _faderPosition = Positioned(
        child: _fader,
        left: left,
        right: right,
        top: _ttop,
      );
    });
  }

  void _onHorizontalSwipeAction(DragEndDetails dragDetails) {

    if (dragDetails.primaryVelocity > 0) {
      print("left");
     setFade(Icons.thumb_up, 0, null);
    } else {
      print("right");
      setFade( Icons.thumb_down, 0, null);
    }
    _sliderTransition(dragDetails.primaryVelocity);
  }

    void _onUpSwipeAction(DragEndDetails dragDetails) {
    print("drag");
    if (dragDetails.primaryVelocity <0 ) {
    startFadeInAndOut(_controllerone);
    _controller.forward().then((_) {
      _slider.nextPage(
          duration: Duration(milliseconds: transitionSpeed), curve: _pageCurve);
      _controller.reverse();
    });
     setFade(Icons.shopping_basket, 0,0);

    }


  }

  void _sliderTransition(double primaryVelocity) {
    this.startFadeInAndOut(_controllerone);
    if (primaryVelocity > 0) {
      _slider.previousPage(
          duration: Duration(milliseconds: transitionSpeed), curve: _pageCurve);
    } else {
      _slider.nextPage(
          duration: Duration(milliseconds: transitionSpeed), curve: _pageCurve);
    }
  }

  void _pageChangeEvent(int page) {
    setState(() {
      calclulateChairlist(page);
    });
  }

  @override
  void initState() {
    super.initState();
    _controller = new AnimationController(
        vsync: this, duration: new Duration(milliseconds: 300));
    _controllerone = new AnimationController(
        vsync: this, duration: new Duration(milliseconds: 299));
    _fader = FadeContainer(
        alignment: FractionalOffset.center,
        animation: _controllerone,
        child: new Container(
          child: Icon(Icons.ac_unit, size: iconSize),
        ));

    _faderPosition = Positioned(
      child: _fader,
      left: 0,
      top: _ttop,
    );
  }

  @override
  Widget build(BuildContext context) {
    var mainAppBar = AppBar(
      title: Text(widget.title),
    );
    var screenSize = MediaQuery.of(context).size.height - iconSize * 3;
    mainAppBar.preferredSize.height;
    _slider = CarouselSlider(
        height: screenSize,
        items: _generateImageList(),
        viewportFraction: 2.0,
        onPageChanged: (page) {
          _pageChangeEvent(page);
        });
    return Scaffold(
      appBar: mainAppBar,
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Stack(
              children: <Widget>[
                Stack(
                  children: <Widget>[_slider, _faderPosition],
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
